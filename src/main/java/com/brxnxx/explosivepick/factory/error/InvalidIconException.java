package com.brxnxx.explosivepick.factory.error;

import com.brxnxx.explosivepick.ExplosivePick;
import org.bukkit.entity.Player;

public class InvalidIconException extends Exception {

    private String message = ExplosivePick.getInstance().getConfig().getString("invalid-icon");

    public String toString() {
        return message.replace("&", "§");
    }

    public void sendToPlayer(Player target) {
        target.sendMessage(toString());
    }

}

package com.brxnxx.explosivepick.factory.manager;

import com.brxnxx.explosivepick.ExplosivePick;
import org.bukkit.Material;

import java.util.ArrayList;
import java.util.List;

public class MaterialsManager {

    private List<Material> loadedMaterials;

    public MaterialsManager() {
        loadMaterials();
    }

    public void loadMaterials() {
        this.loadedMaterials = new ArrayList<>();
        Material material;

        List<Integer> materialString = ExplosivePick.getInstance().getConfig().getIntegerList("Materiais.Ids");

        for (int id : materialString) {
            material = Material.getMaterial(id);
            loadedMaterials.add(material);
        }

    }

    public List<Material> getLoadedMaterials() {
        return loadedMaterials;
    }

}

package com.brxnxx.explosivepick.factory.manager;

import com.brxnxx.explosivepick.ExplosivePick;
import com.brxnxx.explosivepick.factory.error.InvalidIconException;
import com.brxnxx.explosivepick.factory.objects.Pick;
import org.bukkit.Material;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class PickManager {

    private List<Pick> loadedPicks;

    public PickManager() {
        loadPickaxes();
    }


    public void loadPickaxes() {
        loadedPicks = new ArrayList<>();
        Material material;

        for (String picks : ExplosivePick.getInstance().getConfig().getConfigurationSection("Picaretas").getKeys(false)) {
            int id = ExplosivePick.getInstance().getConfig().getInt("Picaretas." + picks + ".Id");
            int radius = ExplosivePick.getInstance().getConfig().getInt("Picaretas." + picks + ".Raio");
            String name = ExplosivePick.getInstance().getConfig().getString("Picaretas." + picks + ".Nome");
            int idItem = ExplosivePick.getInstance().getConfig().getInt("Picaretas." + picks + ".ItemId");

            material = Material.getMaterial(idItem);

            if (material == null) {
                ExplosivePick.getInstance().getLogger().warning("Erro ao carregar o item " + idItem + " da picareta " + picks);
                continue;
            }

            try {
                Pick pick = new Pick(name, material, id, radius);
                loadedPicks.add(pick);
            } catch (InvalidIconException e) {
                e.printStackTrace();
            }
        }
    }

    public Pick getPickFromId(int id) {
        return loadedPicks.stream()
                .filter(pick -> pick.getId() == id)
                .findFirst()
                .orElse(null);
    }

    public Pick getPickFromIcon(Material icon) {
        return loadedPicks.stream()
                .filter(pick -> pick.getIcon().equals(icon))
                .findFirst()
                .orElse(null);
    }

    public Pick getPickFromName(String name) {
        return loadedPicks.stream()
                .filter(pick -> pick.getName().equals(name))
                .findFirst()
                .orElse(null);
    }

    public Pick getAnyPickaxe() {
        Optional<Pick> anyPickaxe = loadedPicks.stream().findAny();
        return anyPickaxe.orElse(null);
    }

    public List<Pick> getLoadedPicks() {
        return loadedPicks;
    }
}

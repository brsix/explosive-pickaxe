package com.brxnxx.explosivepick.factory.objects;

import com.brxnxx.explosivepick.factory.error.InvalidIconException;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

@Getter
public class Pick {

    private int id;
    private int radius;
    private String name;
    private List<String> lore;
    private Material icon;

    private ItemStack item;

    public static List<Material> pickaxes = new ArrayList<>(Arrays.asList(
            Material.DIAMOND_PICKAXE,
            Material.IRON_PICKAXE,
            Material.STONE_PICKAXE,
            Material.WOOD_PICKAXE,
            Material.GOLD_PICKAXE
    ));

    public Pick(String name, Material icon, int radius) throws InvalidIconException {

        if (!pickaxes.contains(icon))
            throw new InvalidIconException();

        this.id = new Random().nextInt(999);
        this.radius = radius;
        this.name = name;
        this.icon = icon;
        this.lore = new ArrayList<>(Arrays.asList(
                "",
                " §6Picareta Explosiva",
                " §6 • Id: §e" + id,
                " §6 • Raio de blocos: §e" + radius,
                "",
                " §6Essa picareta quebra todos os minérios",
                " §6em sua volta no raio de §e" + radius + " §6blocos.",
                ""
        ));
    }

    public Pick(String name, Material icon, int id, int radius) throws InvalidIconException {

        if (!pickaxes.contains(icon))
            throw new InvalidIconException();

        this.id = id;
        this.radius = radius;
        this.name = name;
        this.icon = icon;
        this.lore = new ArrayList<>(Arrays.asList(
                "",
                " §6Picareta Explosiva",
                " §6 • Id: §e" + id,
                " §6 • Raio de blocos: §e" + radius,
                "",
                " §6Essa picareta quebra todos os minérios",
                " §6em sua volta no raio de §e" + radius + " §6blocos.",
                ""
        ));
    }


    public Pick(String name, Material icon) throws InvalidIconException {

        if (!pickaxes.contains(icon))
            throw new InvalidIconException();

        this.id = new Random().nextInt(999);
        this.name = name;
        this.icon = icon;
        this.lore = new ArrayList<>(Arrays.asList(
                "",
                " §6Picareta Explosiva",
                " §6 • Id: §e" + id,
                " §6 • Raio de blocos: §e" + radius,
                "",
                " §6Essa picareta quebra todos os minérios",
                " §6em sua volta no raio de §e" + radius + " §6blocos.",
                ""
        ));
    }

    public Pick setRadius(int radius) {
        this.radius = radius;
        return this;
    }

    public ItemStack returnAsItem() {
        item = new ItemStack(icon);
        ItemMeta itemMeta = item.getItemMeta();

        itemMeta.setDisplayName(name.replace("&", "§"));
        itemMeta.setLore(lore);

        item.setItemMeta(itemMeta);

        return item;
    }

}

package com.brxnxx.explosivepick.minecraft.listeners;

import com.brxnxx.explosivepick.ExplosivePick;
import com.brxnxx.explosivepick.factory.manager.PickManager;
import com.brxnxx.explosivepick.factory.objects.Pick;
import com.sk89q.worldedit.bukkit.BukkitAdapter;
import com.sk89q.worldguard.WorldGuard;
import com.sk89q.worldguard.bukkit.WorldGuardPlugin;
import com.sk89q.worldguard.bukkit.event.block.BreakBlockEvent;
import com.sk89q.worldguard.protection.flags.Flags;
import com.sk89q.worldguard.protection.regions.RegionQuery;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.inventory.ItemStack;

import java.util.List;

public class PickEvents implements Listener {

    @EventHandler
    public void onBreakEvent(BlockBreakEvent event) {
        if (event.getPlayer().getGameMode().equals(GameMode.CREATIVE)) return;
        //if (!canBuild(event.getPlayer(), event.getBlock())) return;

        ItemStack item = event.getPlayer().getItemInHand();

        if (!Pick.pickaxes.contains(item.getType())) return;

        if (item.hasItemMeta()) {
            if (item.getItemMeta().hasLore()) {
                Pick pickaxe = ExplosivePick.getInstance().getManager().getPickFromName(item.getItemMeta().getDisplayName());

                if (pickaxe == null) return;
                if (!ExplosivePick.getInstance().getMaterialsManager().getLoadedMaterials().contains(event.getBlock().getType())) return;

                breakBlocksInCube(event.getPlayer(), event.getBlock(), pickaxe.getRadius());
            }
        }
    }

    private void breakBlocksInCube(Player player, Block start, int radius) {
        Material type = start.getType();
        if (radius < 0)
            return;

        for (int x = -radius; x <= radius; x++) {
            for (int y = -radius; y <= radius; y++) {
                for (int z = -radius; z <= radius; z++) {
                    /**if (!canBuild(player, start)) {
                        return;
                    }*/

                    if (ExplosivePick.getInstance().getMaterialsManager().getLoadedMaterials().contains(start.getType())) {
                        start.getRelative(x, y, z).setType(Material.AIR);
                    }

                    start.getRelative(x, y, z).breakNaturally();
                }
            }
        }
    }

    /**private boolean canBuild(Player p, Block b) {
        RegionQuery query = WorldGuard.getInstance().getPlatform().getRegionContainer().createQuery();
        com.sk89q.worldedit.util.Location loc = BukkitAdapter.adapt(b.getLocation());
        com.sk89q.worldedit.world.World world = BukkitAdapter.adapt(b.getWorld());

        if (!WorldGuard.getInstance().getPlatform().getSessionManager().hasBypass(WorldGuardPlugin.inst().wrapPlayer(p), world)) {
            return query.testState(loc, WorldGuardPlugin.inst().wrapPlayer(p), Flags.BUILD);
        }

        return false;
    }*/

}

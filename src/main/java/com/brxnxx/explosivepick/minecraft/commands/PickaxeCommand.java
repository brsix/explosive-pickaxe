package com.brxnxx.explosivepick.minecraft.commands;

import com.brxnxx.explosivepick.ExplosivePick;
import com.brxnxx.explosivepick.factory.objects.Pick;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

import java.util.Random;

public class PickaxeCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

        String permission = ExplosivePick.getInstance().getConfig().getString("permission");
        String noPermission = ExplosivePick.getInstance().getConfig().getString("no-permission-message");

        if (sender instanceof ConsoleCommandSender || sender.hasPermission(permission)) {
            if (args.length < 3) {
                sender.sendMessage("§cUse /" + label + "§c give <player> <id>");
                return true;
            }

            if (args[0].equals("give")) {
                Player player = Bukkit.getPlayer(args[1]);

                if (player == null) {
                    sender.sendMessage("§cJogador " + args[1] + " §c não encontrado.");
                    return true;
                }

                String idString = args[2];

                if (idString == null) {
                    sender.sendMessage("§cOcorreu um erro no argumento 2 (" + idString + "§c), acho que ele precisa ser um número.");
                    return true;
                }

                int id = parseInteger(sender, idString);

                if (id > 0) {
                    if (ExplosivePick.getInstance().getManager().getLoadedPicks().contains(ExplosivePick.getInstance().getManager().getPickFromId(id))) {
                        Pick pick = ExplosivePick.getInstance().getManager().getPickFromId(id);
                        player.getInventory().addItem(pick.returnAsItem());
                        sender.sendMessage("§aSucesso!");
                    } else {
                        sender.sendMessage("§cNão foi possível encontrar uma picareta com esse ID (" + id + ")§c");
                        return true;
                    }
                }
            }
        } else
            sender.sendMessage(noPermission.replace("&", "§"));

        return false;
    }

    private int parseInteger(CommandSender sender, String arg) {
        try {
            return Integer.parseInt(arg);
        } catch (NumberFormatException exception) {
            sender.sendMessage("§cErro [0x0" + new Random().nextInt(999) + " " + exception.getMessage() + "§c = Argumento 2 tem que ser número!]");
        }

        return 0;
    }

}

package com.brxnxx.explosivepick;

import com.brxnxx.explosivepick.factory.manager.MaterialsManager;
import com.brxnxx.explosivepick.factory.manager.PickManager;
import com.brxnxx.explosivepick.minecraft.commands.PickaxeCommand;
import com.brxnxx.explosivepick.minecraft.listeners.PickEvents;
import com.sk89q.worldguard.bukkit.WorldGuardPlugin;
import config.Configuration;
import jdk.nashorn.internal.objects.annotations.Setter;
import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

public class ExplosivePick extends JavaPlugin {

    @Getter
    private static ExplosivePick instance;
    private Configuration configuration;

    @Getter
    private PickManager manager;

    @Getter
    private MaterialsManager materialsManager;

    //@Getter
    //private WorldGuardPlugin worldGuardPlugin;

    @Override
    public void onEnable() {
        instance = this;
        loadConfiguration();
        initialize();
    }

    @Override
    public void onDisable() {
        // Plugin shutdown logic
    }

    private void loadConfiguration() {
        configuration = new Configuration();
        configuration.setup(this);
    }

    public void reload() {
        reloadConfig();
        manager.loadPickaxes();
        materialsManager.loadMaterials();
    }

    private void initialize() {
        getCommand("picaretaex").setExecutor(new PickaxeCommand());
        this.manager = new PickManager();
        this.materialsManager = new MaterialsManager();
        //this.worldGuardPlugin = (WorldGuardPlugin) Bukkit.getPluginManager().getPlugin("WorldGuard");
        Bukkit.getPluginManager().registerEvents(new PickEvents(), this);
    }

}

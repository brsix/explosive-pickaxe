package config;

import com.google.common.io.Files;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.Plugin;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;

public class Configuration {

    FileConfiguration config;
    File cfile;

    public void setup(Plugin p) {
        cfile = new File(p.getDataFolder(), "config.yml");
        config = p.getConfig();
        config.options().copyDefaults(true);
        saveConfig();

        if (!p.getDataFolder().exists()) {
            p.getDataFolder().mkdir();
        }

        try {
            config.loadFromString(Files.toString(cfile, Charset.forName("UTF-8")));
        } catch (InvalidConfigurationException | IOException e) {
            e.printStackTrace();
        }

    }

    public FileConfiguration getConfig() {
        return config;
    }

    public void updConfig(String pathString, Object value) {
        config.set(pathString, value);
        saveConfig();
        reloadConfig();

    }

    public void saveConfig() {
        try {
            config.save(cfile);
        } catch (IOException e) {
            Bukkit.getServer().getLogger().severe(ChatColor.RED + "Erro ao salvar a config.yml!");
        }
    }

    public void reloadConfig() {
        config = YamlConfiguration.loadConfiguration(cfile);
    }

}
